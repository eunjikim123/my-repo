import requests
import json
import pandas as pd

url = 'https://mapi.lfmall.co.kr/api/search/v2/categories'

data = json.dumps(
    {"aggs":["saleType","colors","season","styleYear","brandGroup","gender","searchCategory","price","prop1","prop2","size1","size2","benefit","review"],"page":1,"size":40,"tid":[80047],"pid":[80104],"order":"popular"}
)

headers = {

    "Content-Type":"application/json;charset=UTF-8"

}

# 데이터 받아오기
response = requests.post(url, data=data, headers=headers)
#print(response.status_code)

products = response.json()['results']['products']
result = []
for i in range(40):
    name = products[i]['name']
    brandName = products[i]['brandName']
    originalPrice = products[i]['originalPrice']
    result.append([name,brandName,originalPrice])
    
#print(response.json()['results']['products'][0]['name'])
#print(response.json()['results']['products'][0]['brandName'])
#print(response.json()['results']['products'][0]['originalPrice'])

df = pd.DataFrame(data = result, columns=['name','brandName','originalPrice'])
#print(df.head())

df.to_csv('test.csv', encoding='utf-8-sig')