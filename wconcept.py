from bs4 import BeautifulSoup
import requests
import sqlite3

conn = sqlite3.connect('test.db', isolation_level=None)
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS table1 \
    (name text, brand text, price text)")

url = 'https://www.wconcept.co.kr/Men/001001'

response = requests.get(url)

html = response.text
soup = BeautifulSoup(html, 'html.parser')
#print(soup)

#li = soup.find_all('div', attrs={'class':'product ellipsis multiline'})[0].getText()
total_li = soup.find_all('div', attrs={'class':'product ellipsis multiline'})

#total_li = soup.select('div.thumbnail_list>ul')
#total_li = soup.find_all('div', attrs={'class':'thumbnail_list'})
#print(total_li)



for i in range(0, len(total_li)):
    name = soup.find_all('div', attrs={'class':'product ellipsis multiline'})[i].getText()
    brand = soup.find_all('div', attrs={'class':'brand'})[i].getText()
    price = soup.find_all('span', attrs={'class':'discount_price'})[i].getText()
    params = (name,brand,price)
    #print(name,brand,price)
    c.execute('INSERT INTO table1 (name,brand,price) Values(?,?,?)',params)
   
#c.execute('select * from table1')
print(c.fetchall())

conn.close()