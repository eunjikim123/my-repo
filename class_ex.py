# 과제3
'''
모든 운동선수는 이름(name)과 체력(stamina)이 있다.
모든 운동선수는 자기를 소개(introduce)할 수 있다.
소개는 자신의 이름을 출력한다. (ex. "My name is John.")
모든 운동선수는 달릴(run) 수 있다.
달리기를 하면 "Run."을 출력한 후 체력을 10 소진한다. 체력이 부족할 경우 "Can't run."을 출력한다.
사격 선수는 예외
모든 운동선수는 휴식(rest)할 수 있다.
달리기를 하면 "Rest." 을 출력한 후 체력을 회복한다.


1) 축구선수(SoccerPlayer) 클래스
축구선수는 킥(kick)을 할 수 있다.
킥을 하면 "Kick."을 출력한 후 체력을 20 소진한다. 체력이 부족할 경우 "Can't kick."을 출력한다.
축구선수의 전체 수(population)를 확인할 수 있어야 한다.


2) 수영선수(Swimmer) 클래스
수영선수는 수영(swim)을 할 수 있다.
수영을 하면 "Swim."을 출력한 후 체력을 30 소진한다. 체력이 부족할 경우 "Can't swim."을 출력한다.
수영선수의 전체 수(population)를 확인할 수 있어야 한다.


3) 사격선수(Shooter) 클래스
사격선수는 사격(shoot)을 할 수 있다.
사격을 하면 "Shoot."을 출력한 후 체력을 5 소진한다. 체력이 부족할 경우 "Can't shoot."을 출력한다.
사격선수는 달리는 것을 싫어한다. 
달리기를 하면 "I hate running."을 출력한다.
사격선수의 전체 수(population)를 확인할 수 있어야 한다.
'''
class main ():
   
    def __init__(self,name,stamina):
        self.name = name
        self.stamina = int(stamina)
        self.origin = int(stamina)
        
    #자기소개
    def introduce(self):
        return "My name is {}".format(self.name)
    
    #휴식
    def rest(self):
        self.stamina = self.origin
        return "rest"


    # 달리기
    def run(self):
        
        if self.stamina <= 0:
            return "Can't run"
        else:
            self.stamina -= 10
            main.rest(self)
            #print(f"reset!{self.stamina}")

            # 달리기 하면 휴식 (원래 stamina로 회복)
            return "run"
            #return f" Run! and my stamina remains{self.stamina}"

        
        
# 축구        
class SoccerPlayer(main):
    
    soccer_num = 0
    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        # popluation 체크
        SoccerPlayer.soccer_num +=1
        
    # 차기
    def kick(self):
        if self.stamina <= 0:
            return "Can't kick "
        else:
            self.stamina -= 20
            return "kick"
            #return "My stamina remains {}".format(self.stamina)

    # 총 인원
    def population():
        return SoccerPlayer.__dict__["soccer_num"]

# 수영
class Swimmer(main):
    swim_num = 0
    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        Swimmer.swim_num +=1
    
    # 수영하기
    def swim(self):
        
        if self.stamina <= 0:
            return "Can't swim"
        else:
            self.stamina -= 30
            return "swim"
        #return f"My stamina remains {self.stamina}"

    # 총 인원    
    def population():
        return Swimmer.__dict__["swim_num"]

# 사격
class Shooter(main):
    shoot_num = 0
    def __init__(self, name, stamina):
        super().__init__(name, stamina)
        Shooter.shoot_num +=1
    
    # 총쏘기
    def shoot(self):

        if self.stamina <= 0:
            return "Can't Shoot"
        else:
            self.stamina -= 30
            return "shoot"
        #return f"My stamina remains {self.stamina}"

    # 총 인원
    def population():
        return Shooter.__dict__["shoot_num"]

    # 달리기는 안해
    def run(self):
        return "I hate running"


# soccer1 = SoccerPlayer(name="John", stamina=30)
# soccer2 = SoccerPlayer(name="Kate", stamina=30)
# shooter1 = Shooter(name="Mark", stamina=30)
# swimmer1 = Swimmer(name = 'Maria', stamina = 40)
# swimmer2 = Swimmer(name = 'JACK', stamina = 20)
# swimmer3 = Swimmer(name = 'Sophia', stamina = 40)
# print(soccer1.introduce())
# print(soccer2.introduce())
# print(SoccerPlayer.population())
# print(shooter1.run())
# print(soccer1.run())
# print(swimmer1.swim())
# print(swimmer1.swim())
# print(swimmer1.swim())
# print(swimmer1.rest())
# print(swimmer1.swim())
# print(Swimmer.population())
